import java.io.*;

public class Main {

    static PrintWriter os;

    public static void main(String[] args) {
        try {
            os = new PrintWriter(new FileOutputStream("output.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        File root = new File(".");
        String tab = "";
        dirPrinter(root, tab);
    }

    public static void dirPrinter(File dir, String tab) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                os.println(tab + "> " + file.getName());
                os.flush();
                dirPrinter(file, tab + "\t");
            } else {
                os.println(tab + "* " + file.getName());
                os.flush();
            }
        }
    }
}
